# pictures

k-9 Mail muss ich freitag suchen, müsste ich dahaben.
Xournal letztes update 2021 ganz schön alt.

## aus euren wünschen

| was ich habe  | ort  | größe |
|---|---|---|
| godot  | zwickau  |20 x 20 |
| codeberg  | zwickau  |20 x 20 |
| digitalcourage | zwickau  |20 x 20 |
| schildichat | zwickau  |20 x 20 |
| Catima | zwickau  |20 x 20 |
| Element | zwickau  |20 x 20 |
| F-Droid | Berlin  |20 x 20 |
| F-Droid Classic (?) | zwickau  |20 x 20 |
| Aegis | zwickau  |20 x 20 |
| joplin | zwickau  |20 x 20 |
| street complete | berlin  |20 x 20 |
| pixelfed | berlin  |20 x 20 |
| periodical | berlin  |20 x 20 |
| tor | berlin  |20 x 20 |
| mumble | berlin  |24 x 30 |
| termux | berlin  |20 x 20 |
| fedilab | berlin  |20 x 20 |
| pixeldroid | nicht   |gemalt |
| home assistant | zwickau  |20 x 20 |
| exodus privacy | berlin  |20 x 20 |
| transportr | berlin  |20 x 20 |
| osmand | berlin  |20 x 20 |
| debian | berlin  |24 x 30 |
| microG | berlin  |20 x 20 |
| zapp | berlin  |20 x 20 |
| newpipe | berlin  |20 x 20 |
| trackercontrol | berlin  |20 x 20 |
| shattered pixel dungeon | nicht  |gemalt |
| DAVx | nicht  gemalt |
| opnsense | nicht   |gemalt |
| wireshark | nicht   |gemalt |
| kanboard | nicht   |gemalt |
| ansible | nicht   |gemalt |
| hedgedoc | nicht   |gemalt |
| Tusky | berlin  |20 x 20 |
| Etar | berlin  |20 x 20 |
| Binary Eye | berlin  |20 x 20 |
| Linux Mint  | berlin  |24 x 30 |
| gitlab | nicht   |gemalt |
| Nextcloud | berlin   |20 x 20 |
| Red Moon | nicht   |gemalt |
| AntennaPod | nicht   |gemalt |
| Infinity for Reddit | nicht   |gemalt |

## habe ich noch hier rumliegen
### würdet ihr welche von diesen ausstellen?

| was ich habe  | HG Farbe  | größe |
|---|---|---|
| Watomatic | blau  |20 x 20 |
| Simple Dialer | orange  |20 x 20 |
| aTox | blau  |20 x 20 |
| OpenDNS Updater | orange  |20 x 20 |
| Badge Magic | rot  |gemalt |
| hEARtest  | orange  |gemalt |
| Delist | weiss  |gemalt |
| droidVNC-NG | weiss  |gemalt |
| Wakelock  | türkis  |gemalt |
| Simple File Manager Pro | orange  |gemalt |
| Woodpecker CI | weiss  |gemalt |
| PSLab | weiss  |gemalt |
| Miniflutt | blau  |gemalt |
| Simple Calculator | orange  |gemalt |
| drip | lila  |gemalt |
| Clover | grün  |gemalt |
| Unit Converter Ultimate | blau  |gemalt |
| LRC Editor | orange  |gemalt |
| X11-Basic | schwarz  |gemalt |
| BLW - Bitcoin Lightning Wallet | blau  |gemalt |
| μlogger | blau  |gemalt |
| MIDICtrl | blau  |gemalt |
| Daily Dozen | grün  |gemalt |
| Docker | weiß  |gemalt |
| Loop Habit Tracker | blau  |gemalt |
| VoIP.ms SMS | rot  |gemalt |
| OPMT | bunt  |gemalt |
| Simple SMS Messenger | orange  |gemalt |
| Android version | orange  |gemalt |
| TSCH_BYL | gelb  |gemalt |
| audioserve | türkis  |gemalt |
| WaniDoku | lila  |gemalt |
| Copyleft | weiß  |gemalt |
| Tinc App | blau  |gemalt |
| 2 Player Battle | lila  |gemalt | godot game
| Photok | türkis  |gemalt |
| Seshat | weiss  |gemalt |
| K-9 Mail | schwarz  |gemalt |
| Block Puzzle Stone Wars | weiss  |gemalt |
| Raise To Answer | weiss  |gemalt |
| Manyverse | blau  |gemalt |
| TinyKeePass  | grün  |gemalt |
| HomeBot  | blau  |gemalt |
| DeuFeiTage  |   |gemalt |
| DAPNET  | weiss  |gemalt |
| Torrent Client  | weiss  |gemalt |
| MetaGer Suche  | orange  |gemalt |
| Timer +X  | lila  |gemalt |
| Mute Reminder  | blau  |gemalt |
| Ruslin  | weinrot  |gemalt |
| Openreads  | türkis  |gemalt |
| Diaguard: Diabetes Tagebuch  | grün  |gemalt |
| falling blocks  | bunt  |gemalt | godot game
| OpenCanteen  | lila  |gemalt |
| F-Droid Classic  | weiss  |gemalt |
| Codeberg  | weiss  |gemalt |
| godot  | blau  |gemalt |
| Fertility Test Analyzer App  | türkis  |gemalt |
| Monero Wallet  | rot  |gemalt |
| Mondstern Acrylic Icons  | weiss  |gemalt |
| Home Assistant  | blau  |gemalt |
| Joplin  | blau  |gemalt |
| Aegis Authenticator  | weiss  |gemalt |
| Element  | grün  |gemalt |
| Catima  | blau  |gemalt |
| Schildichat  | weiss  |gemalt |
| Digitalcourage  | weiss  |gemalt |
| Discreet Launcher  | weiss  |gemalt |
| HTTP Request Shortcuts  | blau  |gemalt |
| Feeel  | blau  |gemalt |
| PCAPdroid  | lila  |gemalt |
| Geonottes  | grün  |gemalt |
| BoltOn  | rot  |gemalt |
| AnonAddy   | blau  |gemalt |
| monocles browser  | weiss  |gemalt |
| Fairemail  | blau  |gemalt |
| OpenTodoList 🕶  | weiss  |gemalt |
| Screenshot Tile  | lila  |gemalt |
| Baby Dots  | weiss  |gemalt |
| Guerrilla Mail  | lila  |gemalt |
| PartyGames  | weiss  |gemalt |
| Transistor - Simple Radio-App  | rot  |gemalt |
| OCReader  | blau  |gemalt |
| blabber.im  | orange  |gemalt |
| Balance the Ball  | grün  |gemalt |
| // foss.events  | weiss  |gemalt |
| Remote Numpad  | weiss  |gemalt |
| Nextcloud Cookbook  | blau  |gemalt |
| wger  | blau  |gemalt |
| Aurora Droid  | blau  |gemalt |
| JioFi Battery Notifier  | lila  |gemalt |
| FlorisBoard  | grau  |gemalt |
| Peercoin Wallet  | grün  |gemalt |
| Open Mensa  | grün  |gemalt |
| KFZ-Kennzeichen  |   |gemalt |
| oRing - Reminder  | weiss  |gemalt |
| Bahnhofsfotos  | weinrot  |gemalt |
| OSM Dashboard Offline für OpenTracks  | grün  |gemalt |
| Friendica  | weiss  |gemalt |
| Trackbook - Movement Recorder  | rot  |gemalt |
| MediLog  | weiss  |gemalt |
| Exif Thumbnail Adder  | blau  |gemalt |
| Mobilizon  | orange  |gemalt |
| Currencies: Währungsrechner  | grün  |gemalt |
| F-Droid Nearby  | blau  |gemalt |
| NiceFeed  | orange  |gemalt |
| Grocy: Self-hosted Grocery Management  | weiss  |gemalt |
| Simple Gallery Pro  | orange  |gemalt |
| InstaLate  | grau  |gemalt |
| ShoppingList  | blau  |gemalt |
| F-Droid Build-Status  | lila  |gemalt |
| Subz  | grün  |gemalt |
| Parlera — word guessing game  | braun  |gemalt |
| deedum gemini browser   | orange  |gemalt |
| ANOTHERpass  | lila  |gemalt |
| To Don't  | gelb  |gemalt |
| extcloud Cookbook  | blau  |gemalt |
| Open Camera  | blau  |gemalt |
| AVNC  | blau  |gemalt |
| Nextcloud Yaga  | blau  |gemalt |
| Home App | For Philips Hue, Arduino  | blau  |gemalt |
| Feeder  | grün  |gemalt |
| Just Another Workout Timer  | blau  |gemalt |
| Chubby Click - Metronome  | weiss  |gemalt |
| Collabora Office  | weiss  |gemalt |
| usageDirect  | blau  |gemalt |
| Museum of a broken API  | blau  |gemalt |
| Wetter  | blau  |gemalt |
| Voltage Drop Calculator  | gelb  |gemalt |
| Runner  | grün  |gemalt |
| FiSSH  | blau  |gemalt |

